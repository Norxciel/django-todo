# from django.shortcuts import render

from rest_framework.response import Response
from rest_framework.decorators import api_view

from src.models import Todo
from src.serializers import TodoSerializer

# Create your views here.

@api_view(['GET'])
def getTodoes(req):
    todoes = Todo.objects.all()

    serializer = TodoSerializer(todoes, many=True)

    return Response(serializer.data)

@api_view(['POST'])
def addTodo(req):
    serializer = TodoSerializer(data=req.data)

    if serializer.is_valid():
        serializer.save()


    return Response(serializer.data)