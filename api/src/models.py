from django.db import models

# Create your models here.

class Todo(models.Model):
    title = models.CharField(max_length=255)
    desc = models.CharField(max_length=255, default='')
    done = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)