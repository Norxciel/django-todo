from django.urls import path
from src import views as srcViews

urlpatterns = [
    path('', srcViews.getTodoes),
    path('add/', srcViews.addTodo)
]